#include <rf60Xsdk.h>
#include <rf60Xtypes.h>

#include <iostream>

using namespace SDK;
using namespace SCANNERS;
using namespace RF60X;

int main() {
  // Initialize sdk library
  sdk_init();

  // Print return rf627 sdk version
  std::cout << sdk_version() << std::endl;

  config_base_information_rf60x_t config_params_device;

  config_params_device.type = SCANNER_TYPE::RF60x;
  config_params_device.protocol = PROTOCOLS::RIFTEK_BIN_PROTOCOL;
  config_params_device.baud_rate = BAUR_RATE_UART::Baud460800;
  config_params_device.network_address = 1;
  config_params_device.number_serial_port = "COM6";

  std::shared_ptr<rf60x> dev = std::make_shared<rf60x>(config_params_device);

  //    dev->connect();
  //    udp_measure_t measure;
  //    for (size_t i = 0; i < 1000; i++) {
  //      auto result = dev->get_measure_udp(&measure);

  //      if (result == false) {
  //        std::cout << "false" << std::endl;
  //      } else {
  //        std::cout << "HELLO  ="
  //                  << static_cast<double>((measure.rf60xValArray[0].value *
  //                                          measure.deviceMeasureRange) /
  //                                         16384.0)
  //                  << std::endl;
  //      }
  //    }

  //    dev->disconnect();

  std::cout << dev->open_port_uart() << std::endl;

  dev->change_config_port_uart();

  //  dev->read_params_uart();

  //    param_t* param =
  //        dev->get_param(PARAM_NAME_KEY::ZERO_BYTE_OF_SOURCE_IP_ADDRESS);
  //    if (param->type ==
  //        param_value_types[(uint32_t)PARAM_VALUE_TYPE::UINT8_PARAM_TYPE]) {
  //      uint8_t test = param->get_value<value_uint8>();
  //      std::cout << test << std::endl;

  //      param->set_value<value_uint8>(128);
  //      dev->set_param(param);
  //      dev->write_params_uart();
  //    }

  // param = dev->get_param(PARAM_NAME_KEY::NETWORK_ADDRESS_UART);
  //   if (param->type ==
  //       param_value_types[(uint32_t)PARAM_VALUE_TYPE::UINT8_PARAM_TYPE]) {
  //     uint8_t test = param->get_value<value_uint8>();
  //     std::cout << test << std::endl;

  //     param->set_value<value_uint8>(3);
  //     dev->set_param(param);
  //     dev->write_params_uart();
  //   }

  dev->send_command(COMMAND_UART::START_STREAM);
  uart_stream_measure_t measure;
  for (int i = 0; i < 1000; ++i) {
    if (dev->get_measure_uart(&measure)) {
      std::cout << "Measure : "
                << static_cast<double>(measure.value * 100.0 / 16384)
                << "  status : " << static_cast<uint32_t>(measure.status)
                << std::endl;
    }

    else {
      std::cout << "false" << std::endl;
    }
  }
  dev->send_command(COMMAND_UART::STOP_STREAM);

  dev->close_port_uart();
}
