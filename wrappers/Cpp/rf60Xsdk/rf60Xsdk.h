#pragma once

#include <iostream>
#include <memory>
#include <vector>

#include "rf60Xtypes.h"

#if (defined _WIN32 && defined RF60X_LIBRARY)
#define API_EXPORT __declspec(dllexport)
#else
#define API_EXPORT
#endif

namespace SDK {
namespace SCANNERS {
namespace RF60X {

/**
 * @brief sdk_version - Return info about SDK version
 * @return SDK version
 */
API_EXPORT int sdk_version();

/**
 * @brief sdk_init - Initialize sdk library
 * Must be called once before further calls to any library functions
 * @return true if success.
 */
API_EXPORT bool sdk_init();

/**
 * @brief sdk_cleanup - Cleanup resources allocated with sdk_init() function
 */
API_EXPORT void sdk_cleanup();

class API_EXPORT rf60x {
 public:
  bool connect();

  bool disconnect();

  bool get_measure_udp(void *measure);

  bool open_port_uart();

  uart_hello_t hell_msg_uart();

  void change_config_port_uart();

  void close_port_uart();

  bool get_measure_uart(void *measure);

  bool read_params_uart();

  bool write_params_uart();

  bool send_command(COMMAND_UART CMD);

  void flash_params_to_scanner_uart();

  param_t* get_param(std::string param_name);
  param_t* get_param(PARAM_NAME_KEY param_name);

  bool set_param(param_t* param);
  bool set_param(const char* param_name, ...);
  bool set_param(int param_id, ...);

  rf60x(const config_base_information_rf60x_t &config);
  rf60x(const rf60x&& obj) = delete;
  rf60x(const rf60x& obj) = delete;
  rf60x& operator=(const rf60x& x) = delete;
  rf60x& operator=(rf60x&&) = delete;
  ~rf60x();

 private:
  void* scanner_base = nullptr;
  config_base_information_rf60x_t current_config;
};
}  // namespace RF60X
}  // namespace SCANNERS
}  // namespace SDK
