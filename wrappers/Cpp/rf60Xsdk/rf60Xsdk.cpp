#include "rf60Xsdk.h"

#include <algorithm>
#include <iostream>
#include <memory>

#include "rf60Xcore.h"

extern "C" {
#include <rf60X_sdk.h>
}

#ifdef _WIN32
#include <winsock.h>
#else
#include <arpa/inet.h>
#endif

extern BOOL EnumAdapterAddresses();
extern void FreeAdapterAddresses();
extern int GetAdaptersCount();
extern const char *GetAdapterAddress(int index);
/* windows sockets tweaks */
extern BOOL WinSockInit();
int SDK::SCANNERS::RF60X::sdk_version() {
  /*
   * Get rf60X core version
   */
  return SDK::CORES::RF60X::version();
}

bool SDK::SCANNERS::RF60X::sdk_init() {
  SDK::CORES::RF60X::init();
  return true;
}

namespace SDK {
namespace SCANNERS {
namespace RF60X {

param_t *create_param_from_type(std::string type) {
  param_t *p = nullptr;
  if (type == pvtKey[PVT_BYTE]) {
    p = new value_uint8();
    p->type = type;
  }
  return p;
}

parameter_t *create_parameter_from_type(const rfChar *type) {
  parameter_t *p = NULL;
  if (rf_strcmp(pvtKey[PVT_BYTE], type) == 0) {
    p = (parameter_t *)calloc(1, sizeof(parameter_t));
    p->rawData = memory_platform.rf_calloc(1, sizeof(rfByte));
    p->base.type = pvtKey[PVT_BYTE];
  }
  return p;
}

bool rf60x::connect() {
  bool result = false;

  result =
      connect_to_scanner_udp((scanner_base_t *)scanner_base,
                             kRIFTEK_BIN_PROTOCOL);  // FIXME ошибка конвертации

  return result;
}

bool rf60x::disconnect() {
  bool result = false;
  result = disconnect_from_scanner_udp((scanner_base_t *)scanner_base,
                                       kRIFTEK_BIN_PROTOCOL);
  return result;
}

bool rf60x::get_measure_udp(void *measure) {
  rf60x_network_udp_measure_t *measure_from_scanner =
      get_measure_from_scanner_udp(
          (scanner_base_t *)scanner_base,
          kRIFTEK_BIN_PROTOCOL);  // FIXME исправить для различных сканеров

  if (measure_from_scanner == NULL) {
    return false;
  }

  switch (((scanner_base_t *)scanner_base)->type) {
    case 1: {
      memcpy((udp_measure_t *)measure, measure_from_scanner->rf60x_udp_measure,
             sizeof(udp_measure_t));

      memory_platform.rf_free(measure_from_scanner->rf60x_udp_measure);
      memory_platform.rf_free(measure_from_scanner);
      break;
    }
    case 2: {
      memcpy(measure, measure_from_scanner->rf60xB_udp_measure,
             sizeof(rf60xB_udp_measure_t));

      memory_platform.rf_free(measure_from_scanner->rf60xB_udp_measure);
      memory_platform.rf_free(measure_from_scanner);
      break;
    }
    case 3: {
      break;
    }
  }

  return true;
  /* reinterpret_cast<udp_measure_t *>(
       measure_from_scanner->rf60x_udp_measure);*/
}

bool rf60x::open_port_uart() {
  return ::open_port_uart((scanner_base_t *)scanner_base);
}

uart_hello_t rf60x::hell_msg_uart() {
  uart_hello_t tempHello;

  rf60x_uart_hello_t *tempReturnValue =
      ::hello_msg_uart((scanner_base_t *)scanner_base);
  memory_platform.rf_memcpy(&tempHello, tempReturnValue, sizeof(uart_hello_t));

  memory_platform.rf_free(tempReturnValue);
  return tempHello;
}

void rf60x::change_config_port_uart() {
  config_port_uart((scanner_base_t *)scanner_base);
}

void rf60x::close_port_uart() {
  ::close_port_uart((scanner_base_t *)scanner_base);
}

bool rf60x::get_measure_uart(void *measure) {
  rfUint8 *measure_from_scanner_uart_ti = ::get_measure_uart(
      (scanner_base_t *)scanner_base);  // FIXME исправить для различных
                                        // сканеров

  /* return reinterpret_cast<uart_stream_measure_t *>(
       measure_from_scanner_uart_ti->uart_stream_measure);*/

  if (measure_from_scanner_uart_ti == NULL) {
    return false;
  }

  switch (((scanner_base_t *)scanner_base)->type) {
    case 1: {
      reinterpret_cast<uart_stream_measure_t *>(measure)->value =
          (measure_from_scanner_uart_ti[0] & 0x0F) |
          (measure_from_scanner_uart_ti[1] & 0x0F) << 4 |
          (measure_from_scanner_uart_ti[2] & 0x0F) << 8 |
          (measure_from_scanner_uart_ti[3] & 0x0F) << 12;
      reinterpret_cast<uart_stream_measure_t *>(measure)->status =
          (measure_from_scanner_uart_ti[0] & 0x40) >> 5;

      memory_platform.rf_free(measure_from_scanner_uart_ti);
      break;
    }
    case 2: {
        reinterpret_cast<rf60x_uart_result_with_encoder_t *>(measure)->value =
            (measure_from_scanner_uart_ti[0] & 0x0F) |
            (measure_from_scanner_uart_ti[1] & 0x0F) << 4 |
            (measure_from_scanner_uart_ti[2] & 0x0F) << 8 |
            (measure_from_scanner_uart_ti[3] & 0x0F) << 12;
        reinterpret_cast<rf60x_uart_result_with_encoder_t *>(measure)
            ->rotationMarks = (measure_from_scanner_uart_ti[4] & 0x0F) |
            (measure_from_scanner_uart_ti[5] & 0x0F) << 4 |
            (measure_from_scanner_uart_ti[6] & 0x0F) << 8 |
            (measure_from_scanner_uart_ti[7] & 0x0F) << 12;
        reinterpret_cast<rf60x_uart_result_with_encoder_t *>(measure)
            ->encoderValue = (measure_from_scanner_uart_ti[8] & 0x0F) |
            (measure_from_scanner_uart_ti[9] & 0x0F) << 4 |
            (measure_from_scanner_uart_ti[10] & 0x0F) << 8 |
            (measure_from_scanner_uart_ti[11] & 0x0F) << 12;

        memory_platform.rf_free(measure_from_scanner_uart_ti);
      break;
    }
    case 3: {
      break;
    }
  }

  return true;
}

bool rf60x::read_params_uart() {
  return read_params_from_scanner_uart((scanner_base_t *)scanner_base);
}

bool rf60x::write_params_uart() {
  return write_params_to_scanner_uart((scanner_base_t *)scanner_base);
}

bool rf60x::send_command(COMMAND_UART CMD) {
  return send_command_uart((scanner_base_t *)scanner_base,
                           static_cast<rf60x_command_t>(CMD));
}

void rf60x::flash_params_to_scanner_uart() {}

param_t *rf60x::get_param(std::string param_name) {
  parameter_t *p =
      get_parameter((scanner_base_t *)this->scanner_base, param_name.c_str());
  if (p != NULL) {
    param_t *result = create_param_from_type(std::string(p->base.type));

    if (result->type == pvtKey[PVT_BYTE]) {
      result->set_value<value_uint8>(p->val_uint8->value);
      result->name = p->base.name;
      result->access = p->base.access;
      result->index = p->base.index;
      result->offset = p->base.offset;
      result->size = p->base.size;
      result->units = p->base.units;

      ((value_uint8 *)result)->defaultValue = p->val_uint8->defValue;
      ((value_uint8 *)result)->value = p->val_uint8->value;
      ((value_uint8 *)result)->min = p->val_uint8->min;
      ((value_uint8 *)result)->max = p->val_uint8->max;
    }
    return result;
  }
  return nullptr;
}

param_t *rf60x::get_param(PARAM_NAME_KEY param_name) {
  return get_param(parameter_names[(uint32_t)param_name]);
}

bool rf60x::set_param(param_t *param) {
  parameter_t *p = create_parameter_from_type(param->type.c_str());

  if (p != NULL) {
    p->base.name = param->name.c_str();
    p->base.type = param->type.c_str();
    p->base.access = param->access.c_str();
    p->base.units = param->units.c_str();
    if (param->type == pvtKey[PVT_BYTE]) {
      p->val_uint8->value = param->get_value<value_uint8>();
    }
    set_parameter((scanner_base_t *)this->scanner_base, p);

    return true;
  }
  return false;
}

bool rf60x::set_param(const char *param_name, ...) {
  va_list valist;
  va_start(valist, param_name);

  bool result = set_parameter_by_name((scanner_base_t *)this->scanner_base,
                                      param_name, valist);

  va_end(valist);

  return result;
}

bool rf60x::set_param(int param_id, ...) {
  va_list valist;
  va_start(valist, param_id);

  bool result =
      set_parameter_by_name((scanner_base_t *)this->scanner_base,
                            parameter_names[param_id].c_str(), valist);

  va_end(valist);

  return result;
}

rf60x::rf60x( const config_base_information_rf60x_t &config) : current_config(config) {
  scanner_base_t *scanner_base_ti =
      (scanner_base_t *)memory_platform.rf_calloc(1, sizeof(scanner_base_t));

  scanner_base_ti->type = static_cast<scanner_types_t>(current_config.type);

  scanner_base_ti->rf60x =
      (rf60x_t *)memory_platform.rf_calloc(1, sizeof(rf60x_t));

  // "\\\\.\\" Если значение больше 9

  std::copy(current_config.number_serial_port.begin(),
            current_config.number_serial_port.end(),
            scanner_base_ti->rf60x->portName);

  scanner_base_ti->rf60x
      ->portName[current_config.number_serial_port.size() + 1] = '\0';

  scanner_base_ti->rf60x->uart_info =
      (rf_DCB *)memory_platform.rf_calloc(1, sizeof(rf_DCB));

  scanner_base_ti->rf60x->uart_info->BaudRate =
      (uint32_t)current_config.baud_rate * 2400;

  scanner_base_ti->rf60x->user_params.network_address_uart=current_config.network_address;

  scanner_base = reinterpret_cast<void *>(scanner_base_ti);

}

rf60x::~rf60x() {
  scanner_base_t *scanner_base_ti =
      reinterpret_cast<scanner_base_t *>(scanner_base);
  memory_platform.rf_free(scanner_base_ti->rf60x);
  memory_platform.rf_free(scanner_base_ti);

}

}  // namespace RF60X
}  // namespace SCANNERS
}  // namespace SDK
