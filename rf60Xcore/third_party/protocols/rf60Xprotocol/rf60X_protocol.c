#include "rf60X_protocol.h"


rfUint32 rf60x_protocol_old_pack_payload_msg_to_user_params_packet(rfUint8 *buffer, vector_t *params_list)
{
    rfUint8 *buf = &buffer[0];
    for (rfSize i = 0; i < vector_count(params_list); i++) {
        parameter_t *p = vector_get(params_list, i);
        if (p != NULL) {
            if (rf_strcmp(pvtKey[PVT_BYTE], p->base.type) == 0) {
                memory_platform.rf_memcpy(&buf[p->base.offset], &p->val_uint8->value,
                                          p->base.size);
            }
        }
    }

    return 0;
}



rf60x_udp_measure_t rf60x_protocol_old_unpack_measure_msg_from_packet(rfUint8 *buffer,rf60x_udp_measure_t *msg)
{

    rfUint16 i=0;
    rfUint8 *p = buffer;

     for(i=0; i<168; ++i){
         msg->rf60xValArray[i].value = get_rfUint16_from_packet(&p, kEndianessLittle);
        msg->rf60xValArray[i+1].status = get_rfUint8_from_packet(&p);
    }

    msg->deviceSerial = get_rfUint16_from_packet(&p, kEndianessLittle);
    msg->deviceBaseDistance = get_rfUint16_from_packet(&p, kEndianessLittle);
    msg->deviceMeasureRange = get_rfUint16_from_packet(&p, kEndianessLittle);
    msg->packCount = get_rfUint8_from_packet(&p);
    msg->packetControlSumm=get_rfUint8_from_packet(&p);

    return *msg;
}




