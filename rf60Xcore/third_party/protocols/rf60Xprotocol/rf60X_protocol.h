#ifndef RF60X_PROTOCOL_H
#define RF60X_PROTOCOL_H

#include <string.h>

#include "custom_string.h"
#include "custom_vector.h"
#include "endian_conv.h"
#include "memory_platform.h"
#include "netwok_platform.h"
#include "platform_types.h"

rfUint32 rf60x_protocol_old_pack_payload_msg_to_user_params_packet(
    rfUint8* buffer, vector_t* params_list);

rf60x_udp_measure_t rf60x_protocol_old_unpack_measure_msg_from_packet(
    rfUint8* buffer, rf60x_udp_measure_t* msg);
#endif  // RF60X_PROTOCOL_H
