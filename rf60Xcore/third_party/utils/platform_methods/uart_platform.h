#ifndef UART_PLATFORM_H
#define UART_PLATFORM_H

#include "platform_types.h"

typedef struct {
  rfULInt DCBlength;
  rfULInt BaudRate;
  rfULInt fBinary : 1;
  rfULInt fParity : 1;
  rfULInt fOutxCtsFlow : 1;
  rfULInt fOutxDsrFlow : 1;
  rfULInt fDtrControl : 2;
  rfULInt fDsrSensitivity : 1;
  rfULInt fTXContinueOnXoff : 1;
  rfULInt fOutX : 1;
  rfULInt fInX : 1;
  rfULInt fErrorChar : 1;
  rfULInt fNull : 1;
  rfULInt fRtsControl : 2;
  rfULInt fAbortOnError : 1;
  rfULInt fDummy2 : 17;
  rfUint16 wReserved;
  rfUint16 XonLim;
  rfUint16 XoffLim;
  rfBool ByteSize;
  rfBool Parity;
  rfBool StopBits;
  rfChar XonChar;
  rfChar XoffChar;
  rfChar ErrorChar;
  rfChar EofChar;
  rfChar EvtChar;
  rfUint16 wReserved1;
} rf_DCB;

typedef void *(*create_file_t)(char *COMPortName);

typedef void (*close_file_t)(void *COMPortName);

typedef void (*change_paramaters_t)(void *COMPortName, rf_DCB *DCB);

typedef rfSize (*recv_data_uart_t)(void *handleCOMPORT, void *buffer,
                                   rfUint8 numbeBytesToRead);
typedef rfSize (*send_data_uart_t)(void *COMPortName, const void *buf,
                                   rfSize len);

typedef struct {
  create_file_t create_file;
  close_file_t close_file;
  recv_data_uart_t recv_data_uart;
  send_data_uart_t send_data_uart;
  change_paramaters_t change_paramaters;

} uart_platform_dependent_methods_t;

/** @brief Structure with user-provided platform-specific settings
 */

typedef struct {
  uart_platform_dependent_methods_t uart_methods;
} uart_platform_t;

extern uart_platform_t uart_platform;

void init_uart_platform(uart_platform_dependent_methods_t *methods);

#endif  // UART_PLATFORM_H
