#ifndef RF60X_TYPES_H
#define RF60X_TYPES_H

#include <stddef.h>

#include "platform_types.h"

typedef struct {
  rfUint16 measure;
  rfByte status;
  rfByte reserverd;
} rf60x_uart_stream_measure_t;

typedef struct {
  rfUint16 value;
  rfUint16 rotationMarks;
  rfUint16 encoderValue;
} rf60x_uart_result_with_encoder_t;

typedef struct {
  rfUint16 value;
  rfByte status;
} rf60x_udp_value_t;

typedef struct {
  rf60x_udp_value_t rf60xValArray[168];
  rfUint16 deviceSerial;
  rfUint16 deviceBaseDistance;
  rfUint16 deviceMeasureRange;
  rfByte packCount;
  rfByte packetControlSumm;
} rf60x_udp_measure_t;

typedef struct {
  rfUint16 measure;
  rfUint16 exposition;
  rfByte status;
} rf60xB_udp_value_t;

typedef struct {
  rf60xB_udp_value_t rf60xBValArray[100];
  rfUint16 reserved;
  rfUint16 reserved2;
  rfUint16 deviceSerial;
  rfUint16 deviceBaseDistance;
  rfUint16 deviceMeasureRange;
  rfByte packCount;
  rfByte packetControlSumm;
} rf60xB_udp_measure_t;

typedef struct {
  rfByte deviceType;
  rfByte deviceModificaton;
  rfUint16 deviceSerial;
  rfUint16 deviceMaxDistance;
  rfUint16 deviceRange;

} rf60x_uart_hello_t;

typedef enum { kRF60x = 1, kRF60xHS = 2, kRF60xB = 3 } scanner_types_t;

typedef enum {
  kRIFTEK_BIN_PROTOCOL = 1,
  kASCII = 2,
  kMODBUS_RTU = 3,
} protocol_types_t;

typedef struct {
  scanner_types_t type;
  union {
    rf60x_udp_measure_t* rf60x_udp_measure;
    rf60xB_udp_measure_t* rf60xB_udp_measure;
  };
} rf60x_network_udp_measure_t;

typedef struct {
  rfByte user_power;                                             // 0x00
  rfByte power_analog_output;                                    // 0x01
  rfByte control_of_averaging;                                   // 0x02
  rfByte network_address_uart;                                   // 0x03
  rfByte baud_rate_uart;                                         // 0x04
  rfByte number_of_averaged_values;                              // 0x06
  rfByte low_byte_of_the_sampling_period;                        // 0x08
  rfByte high_byte_of_the_sampling_period;                       // 0x09
  rfByte low_byte_of_max_integration_time;                       // 0x0A
  rfByte high_byte_of_max_integration_time;                      // 0x0B
  rfByte low_byte_for_the_beginning_of_analog_out_range;         // 0x0C
  rfByte high_byte_for_the_beginning_of_analog_out_range;        // 0x0D
  rfByte low_byte_for_the_end_of_analog_out_range;               // 0x0E
  rfByte high_byte_for_the_end_of_analog_out_range;              // 0x0F
  rfByte time_lock_of_result;                                    // 0x10
  rfByte low_byte_of_a_zero_point;                               // 0x17
  rfByte high_byte_of_a_zero_point;                              // 0x18
  rfByte data_transfer_rate_via_CAN_interface;                   // 0x20
  rfByte low_byte_of_standard_identifier;                        // 0x22
  rfByte high_byte_of_standard_identifier;                       // 0x23
  rfByte zero_byte_of_extended_identifier;                       // 0x24
  rfByte first_byte_of_extended_identifier;                      // 0x25
  rfByte second_byte_of_extended_identifier;                     // 0x26
  rfByte third_byte_of_extended_identifier;                      // 0x27
  rfByte CAN_interface_identifier;                               // 0x28
  rfByte CAN_interface_power_switch;                             // 0x29
  rfByte zero_byte_of_destination_ip_address;                    // 0x6C
  rfByte first_byte_of_destination_ip_address;                   // 0x6D
  rfByte second_byte_of_destination_ip_address;                  // 0x6E
  rfByte third_byte_of_destination_ip_address;                   // 0x6F
  rfByte zero_byte_of_gateway_ip_address;                        // 0x70
  rfByte first_byte_of_gateway_ip_address;                       // 0x71
  rfByte second_byte_of_gateway_ip_address;                      // 0x72
  rfByte third_byte_of_gateway_ip_address;                       // 0x73
  rfByte zero_byte_of_subnet_mask;                               // 0x74
  rfByte first_byte_of_subnet_mask;                              // 0x75
  rfByte second_byte_of_subnet_mask;                             // 0x76
  rfByte third_byte_of_subnet_mask;                              // 0x77
  rfByte zero_byte_of_source_ip_address;                         // 0x78
  rfByte first_byte_of_source_ip_address;                        // 0x79
  rfByte second_byte_of_source_ip_address;                       // 0x7A
  rfByte third_byte_of_source_ip_address;                        // 0x7B
  rfByte low_byte_of_the_number_of_measurements_in_the_packet;   // 0x7C
  rfByte high_byte_of_the_number_of_measurements_in_the_packet;  // 0x7D
  rfByte eth_interface_on_off;                                   // 0x88
  rfByte auto_of_the_stream;                                     // 0x89
  rfByte protocols_interface;                                    // 0x8A

} rf60x_uart_params_t;

////////////////////////////////////////////////////////////////////

//Поддерживаемый доступ к параметру
typedef enum {
  PAT_UNKN = 0,
  PAT_READ_ONLY,
  PAT_WRITE,
  PAT_RESTRICTED,
} paramAccessType_t;

typedef enum {
  PVT_UNKN = 0,
  PVT_BYTE,
  PVT_UINT,
  PVT_UINT64,
  PVT_INT,
  PVT_INT64,
  PVT_FLOAT,
  PVT_DOUBLE,
  PVT_ARRAY_UINT32,
  PVT_ARRAY_UINT64,
  PVT_ARRAY_INT32,
  PVT_ARRAY_INT64,
  PVT_ARRAY_FLT,
  PVT_ARRAY_DBL,
  PVT_STRING
} paramValueType_t;

typedef enum {

  WRITEPARAM = 0x03,
  SAVETOFLASH = 0x04,
  RESTOREFORMFLASH = 0x04,
  START_STREAM = 0x07,
  STOP_STREAM = 0x08
} rf60x_command_t;

const static rfChar* pvtKey[] = {
    "unkn_t",    "uint8_t",   "uint32_t",    "uint64_t",    "int32_t",
    "int64_t",   "float_t",   "double_t",    "u32_arr_t",   "u64_arr_t",
    "i32_arr_t", "i64_arr_t", "flt_array_t", "dbl_array_t", "string_t"};

const static rfChar* patKey[] = {
    "unkn",
    "read_only",
    "write",
    "locked",
};
const static rfChar* matKey[] = {
    "unkn",
    "factory_only",
    "user",
};

typedef struct {
  const rfChar* name;
  const rfChar* type;
  const rfChar* access;
  rfUint16 index;
  rfUint32 offset;
  rfUint32 size;
  const rfChar* units;
} value_base_t;

typedef struct value_uint8_t {
  rfUint8 min;
  rfUint8 max;
  rfUint8 step;
  rfUint8 defValue;
  rfUint8 value;

} value_uint8_t;

typedef struct {
  value_base_t base;
  union {
    void* rawData;
    value_uint8_t* val_uint8;
  };
} parameter_t;

const static rfChar* parameter_names_array[] = {
    "user_power",
    "power_analog_output",
    "control_of_averaging",
    "network_address_uart",
    "baud_rate_uart",
    "number_of_averaged_values",
    "low_byte_of_the_sampling_period",
    "high_byte_of_the_sampling_period",
    "low_byte_of_max_integration_time",
    "high_byte_of_max_integration_time",
    "low_byte_for_the_beginning_of_analog_out_range",
    "high_byte_for_the_beginning_of_analog_out_range",
    "low_byte_for_the_end_of_analog_out_range",
    "high_byte_for_the_end_of_analog_out_range",
    "time_lock_of_result",
    "low_byte_of_a_zero_point",
    "high_byte_of_a_zero_point",
    "data_transfer_rate_via_CAN_interface",
    "low_byte_of_standard_identifier",
    "high_byte_of_standard_identifier",
    "zero_byte_of_extended_identifier",
    "first_byte_of_extended_identifie",
    "second_byte_of_extended_identifier",
    "third_byte_of_extended_identifier",
    "CAN_interface_identifier",
    "CAN_interface_power_switch",
    "zero_byte_of_destination_ip_address",
    "first_byte_of_destination_ip_address",
    "second_byte_of_destination_ip_address",
    "third_byte_of_destination_ip_address",
    "zero_byte_of_gateway_ip_address",
    "first_byte_of_gateway_ip_address",
    "second_byte_of_gateway_ip_address",
    "third_byte_of_gateway_ip_address",
    "zero_byte_of_subnet_mask",
    "first_byte_of_subnet_mask",
    "second_byte_of_subnet_mask",
    "third_byte_of_subnet_mask",
    "zero_byte_of_source_ip_address",
    "first_byte_of_source_ip_address",
    "second_byte_of_source_ip_address",
    "third_byte_of_source_ip_address",
    "low_byte_of_the_number_of_measurements_in_the_packet",
    "high_byte_of_the_number_of_measurements_in_the_packet",
    "eth_interface_on_off",
    "auto_of_the_stream",
    "protocols_interface",
};

typedef enum {
  USER_POWER = 0,
  POWER_ANALOG_OUTPUT,
  CONTROL_OF_AVERAGING,
  NETWORK_ADDRESS_UART,
  BAUD_RATE_UART,
  NUMBER_OF_AVERAGED_VALUES,
  LOW_BYTE_OF_THE_SAMPLING_PERIOD,
  HIGH_BYTE_OF_THE_SAMPLING_PERIOD,
  LOW_BYTE_OF_MAX_INTEGRATION_TIME,
  HIGH_BYTE_OF_MAX_INTEGRATION_TIME,
  LOW_BYTE_FOR_THE_BEGINNING_OF_ANALOG_OUT_RANGE,
  HIGH_BYTE_FOR_THE_BEGINNING_OF_ANALOG_OUT_RANGE,
  LOW_BYTE_FOR_THE_END_OF_ANALOG_OUT_RANGE,
  HIGH_BYTE_FOR_THE_END_OF_ANALOG_OUT_RANGE,
  TIME_LOCK_OF_RESULT,
  LOW_BYTE_OF_A_ZERO_POINT,
  HIGH_BYTE_OF_A_ZERO_POINT,
  DATA_TRANSFER_RATE_VIA_CAN_INTERFACE,
  LOW_BYTE_OF_STANDARD_IDENTIFIER,
  HIGH_BYTE_OF_STANDARD_IDENTIFIER,
  ZERO_BYTE_OF_EXTENDED_IDENTIFIER,
  FIRST_BYTE_OF_EXTENDED_IDENTIFIE,
  SECOND_BYTE_OF_EXTENDED_IDENTIFIER,
  THIRD_BYTE_OF_EXTENDED_IDENTIFIER,
  CAN_INTERFACE_IDENTIFIER,
  CAN_INTERFACE_POWER_SWITCH,
  ZERO_BYTE_OF_DESTINATION_IP_ADDRESS,
  FIRST_BYTE_OF_DESTINATION_IP_ADDRESS,
  SECOND_BYTE_OF_DESTINATION_IP_ADDRESS,
  THIRD_BYTE_OF_DESTINATION_IP_ADDRESS,
  ZERO_BYTE_OF_GATEWAY_IP_ADDRESS,
  FIRST_BYTE_OF_GATEWAY_IP_ADDRESS,
  SECOND_BYTE_OF_GATEWAY_IP_ADDRESS,
  THIRD_BYTE_OF_GATEWAY_IP_ADDRESS,
  ZERO_BYTE_OF_SUBNET_MASK,
  FIRST_BYTE_OF_SUBNET_MASK,
  SECOND_BYTE_OF_SUBNET_MASK,
  THIRD_BYTE_OF_SUBNET_MASK,
  ZERO_BYTE_OF_SOURCE_IP_ADDRESS,
  FIRST_BYTE_OF_SOURCE_IP_ADDRESS,
  SECOND_BYTE_OF_SOURCE_IP_ADDRESS,
  THIRD_BYTE_OF_SOURCE_IP_ADDRESS,
  LOW_BYTE_OF_THE_NUMBER_OF_MEASUREMENTS_IN_THE_PACKET,
  HIGH_BYTE_OF_THE_NUMBER_OF_MEASUREMENTS_IN_THE_PACKET,
  ETH_INTERFACE_ON_OFF,
  AUTO_OF_THE_STREAM,
  PROTOCOLS_INTERFACE,
} parameter_name_keys_t;

////////////////////////////////////////////////////////////////////

typedef struct {
  scanner_types_t type;
  union {
    rf60x_uart_stream_measure_t* uart_stream_measure;
    rf60x_uart_result_with_encoder_t *uart_measure_with_encoder;
  };
} rf60x_uart_protocol_t;

#endif  // RF60X_TYPES_H
