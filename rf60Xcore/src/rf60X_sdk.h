#ifndef RF60X_SDK_H
#define RF60X_SDK_H

#include "custom_vector.h"
#include "platform_types.h"
#include "rf60X_devices.h"
#include "rf60X_protocol.h"
#if (defined _WIN32 && defined RF60X_LIBRARY)
#define dllexport __declspec(dllexport)
#else
#define dllexport
#endif

/**
 * @brief change_platform_adapter_settings - change adapter's settings
 * @param adapter_settings
 */
dllexport void set_platform_adapter_settings(rfUint32 host_mask,
                                             rfUint32 host_ip_addr);

/**
 * @brief connect - Establish connection to the RF60X device
 * @param device - port to scanner
 * @param protocol - protocol's type (RIFTEK_PROTOCOL, ASCII, Modbus-RTU)
 * @return 0 on success
 */
dllexport rfUint8 connect_to_scanner_udp(scanner_base_t *device,
                                         protocol_types_t protocol);

/**
 * @brief disconnect_from_scanner - Close connection to the device
 * @param device - prt to scanner
 * @param protocol - protocol's type (RIFTEK_PROTOCOL, ASCII, Modbus-RTU)
 * @return 0 on success
 */
dllexport rfUint8 disconnect_from_scanner_udp(scanner_base_t *device,
                                              protocol_types_t protocol);

/**
 * @brief get_profile - Get measurement from scanner's data stream
 * @param device - port to scanner
 * @param protocol - protocol's type (RIFTEK_PROTOCOL, ASCII, Modbus-RTU)
 * @return ptr to rf60x_result_structure
 */

dllexport rf60x_network_udp_measure_t *get_measure_from_scanner_udp(
    scanner_base_t *device, protocol_types_t protocol);

/**
 * @brief rf60x_open_port_uart - Opens COM port for communication with the
 * device
 * @param scanner - ptr to device
 * @return status of implementation of the method
 */

dllexport rfBool open_port_uart(scanner_base_t *device);

/**
 * @brief rf60x_config_port_uart - Configuring the port configuration to work
 * with it
 * @param scanner - ptr to device
 */
dllexport void config_port_uart(scanner_base_t *device);


/**
 * @brief hello_msg_uart - Checking the availability of the sensor when connected via UART
 * with it
 * @param scanner - ptr to device
 */
dllexport rf60x_uart_hello_t *hello_msg_uart(scanner_base_t *device);


/**
 * @brief get_measure_uart - Receiving measurements via UART
 * with it
 * @param scanner - ptr to device
 */
dllexport rfUint8 *get_measure_uart(scanner_base_t *device);


/**
 * @brief send_command_uart - Sending commands to the sensor via UART
 * with it
 * @param scanner - ptr to device
 */
dllexport rfBool send_command_uart(scanner_base_t *device, rf60x_command_t cmd);

/**
 * @brief close_port_uart - Close the COM port for communication with the device
 * @param scanner - ptr to device
 */
dllexport void close_port_uart(scanner_base_t *device);

/**
 * @brief read_params_from_scanner_uart - Read parameters from device to rfInternal
 * structure. This structure is accessible via get_params() function
 * @param device - ptr to scanner
 * @return 0 on success
 */
dllexport rfUint8 read_params_from_scanner_uart(scanner_base_t *device);

/**
 * @brief write_params_to_scanner_uart - Write current parameters to device's memory
 * @param device - ptr to scanner
 * @return 0 on success
 */
dllexport rfUint8 write_params_to_scanner_uart(scanner_base_t *device);

/**
 * @brief get_parameter - Search parameters by his name
 * @param device - ptr to scanner
 * @param param_name - name of parameter
 * @return param on success, else - null
 */
dllexport parameter_t *get_parameter(scanner_base_t *device,
                                     const rfChar *param_name);

/**
 * @brief set_parameter - Set parameter
 * @param device - ptr to scanner
 * @param param - setting parameter
 * @return 0 if success
 */
dllexport rfUint8 set_parameter(scanner_base_t *device, parameter_t *param);

/**
 * @brief set_parameter_by_name - Set parameters by his name
 * @param device - ptr to scanner
 * @param param_name - parameter name
 * @param value - value
 * @return 0 if success
 */
dllexport rfUint8 set_parameter_by_name(scanner_base_t *device,
                                        const char *param_name, va_list value);

/**
 * @brief set_parameter - Search parameters by his name
 * @param device - ptr to scanner
 * @param param_name - name of parameter
 * @return param on success, else - null
 */
/*dllexport rfUint8 send_command(scanner_base_t *device, command_t *command);*/

#endif  // RF60X_SDK_H
