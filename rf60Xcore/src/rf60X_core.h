#ifndef RF60X_CORE_H
#define RF60X_CORE_H

#include "iostream_platform.h"
#include "memory_platform.h"
#include "netwok_platform.h"
#include "platform_types.h"
#include "uart_platform.h"

#if (defined _WIN32 && defined RF60X_LIBRARY)
#define dllexport __declspec(dllexport)
#else
#define dllexport
#endif

/*! Return rf60X sdk version
 */
dllexport rfUint32 core_version();

/**
 * @brief init_platform_dependent_methods - init platform dependent methods and
 * settings
 * @param memory_methods
 * @param iostream_methods
 * @param network_methods
 * @param adapter_settings
 */
dllexport void init_platform_dependent_methods(
    memory_platform_dependent_methods_t* memory_methods,
    iostream_platform_dependent_methods_t* iostream_methods,
    network_platform_dependent_methods_t* network_methods,
    network_platform_dependent_settings_t* adapter_settings,
    uart_platform_dependent_methods_t* uart_methods);

#endif  // RF60X_CORE_H
