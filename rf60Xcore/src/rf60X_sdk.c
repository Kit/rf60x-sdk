#include "rf60X_sdk.h"

void set_platform_adapter_settings(rfUint32 host_mask, rfUint32 host_ip_addr) {
  set_adapter_settings(host_mask, host_ip_addr);
}

rfUint8 connect_to_scanner_udp(scanner_base_t *device,
                               protocol_types_t protocol) {
  switch (device->type) {
    case kRF60x:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          return rf60x_connect_udp(device->rf60x);
          break;
        case kASCII:
        case kMODBUS_RTU:
          return 1;  //
          break;
        default:
          return 1;  // Unknown protocol type
          break;
      }
      break;
    case kRF60xHS:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
      }
    case kRF60xB:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          return rf60x_connect_udp(device->rf60x);
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
        default:
          return 1;  // Unknown protocol type
          break;
      }
      break;
    default:
      return 2;  // Unknown device type
      break;
  }
  return 0;
}

rfUint8 disconnect_from_scanner_udp(scanner_base_t *device,
                                    protocol_types_t protocol) {
  switch (device->type) {
    case kRF60x:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          rf60x_disconnect_udp(device->rf60x);
          break;
        case kASCII:
        case kMODBUS_RTU:
          return 1;  //
          break;
        default:
          return 1;  // Unknown protocol type
          break;
      }
      break;
    case kRF60xHS:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
      }
    case kRF60xB:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          rf60x_disconnect_udp(device->rf60x);
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
        default:
          return 1;  // Unknown protocol type
          break;
      }
      break;
    default:
      return 2;  // Unknown device type
      break;
  }
  return 0;
}
rf60x_network_udp_measure_t *get_measure_from_scanner_udp(
    scanner_base_t *device, protocol_types_t protocol) {
  rf60x_network_udp_measure_t *measure =
      memory_platform.rf_calloc(1, sizeof(rf60x_network_udp_measure_t));
  switch (device->type) {
    case kRF60x:
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL: {
          rfUint8 *tempPointer = rf60x_get_measure_udp(device->rf60x);

          measure->rf60x_udp_measure =
              memory_platform.rf_calloc(1, sizeof(rf60x_udp_measure_t));
          if (tempPointer == NULL) {
            memory_platform.rf_free(measure->rf60x_udp_measure);
            memory_platform.rf_free(measure);
            memory_platform.rf_free(tempPointer);
            return NULL;
          } else {
            measure->type = kRF60x;
            /*rf60x_udp_measure_t tempMeasure =*/
                rf60x_protocol_old_unpack_measure_msg_from_packet(
                    tempPointer, measure->rf60x_udp_measure);

            memory_platform.rf_free(tempPointer);
            return measure;
          }
        }
        case kASCII:
          break;
        case kMODBUS_RTU:
          return NULL;
          break;
        default:
          return NULL;  // Unknown protocol type
          break;
      }
      break;
    case kRF60xHS:
      measure->type = kRF60xHS;
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
        default:
          return NULL;  // Unknown protocol type
          break;
      }
      break;
    case kRF60xB:
      measure->type = kRF60xB;
      switch (protocol) {
        case kRIFTEK_BIN_PROTOCOL:

          measure->rf60xB_udp_measure =
              (rf60xB_udp_measure_t *)rf60x_get_measure_udp(device->rf60x);
          if (measure->rf60xB_udp_measure == NULL) {
            memory_platform.rf_free(measure);
            return NULL;
          } else {
            measure->type = kRF60xB;
            return measure;
          }
          break;
        case kASCII:
          break;
        case kMODBUS_RTU:
          break;
        default:
          return NULL;  // Unknown protocol type
          break;
      }
      break;
    default:
      return NULL;  // Unknown device type
      break;
  }
  return 0;
}

rfBool open_port_uart(scanner_base_t *device) {
  return rf60x_open_port_uart(device->rf60x);
}

void config_port_uart(scanner_base_t *device) {
  rf60x_config_port_uart(device->rf60x);
}

void close_port_uart(scanner_base_t *device) {
  rf60x_close_port_uart(device->rf60x);
}

rfUint8 read_params_from_scanner_uart(scanner_base_t *device) {
  return rf60x_read_params_from_scanner_uart(device->rf60x);
}

rfUint8 write_params_to_scanner_uart(scanner_base_t *device) {
  return rf60x_write_params_to_scanner_uart(device->rf60x);
}

parameter_t *get_parameter(scanner_base_t *device, const rfChar *param_name) {
  return rf60x_get_parameter(device->rf60x, param_name);
}

rfUint8 set_parameter(scanner_base_t *device, parameter_t *param) {
  return rf60x_set_parameter(device->rf60x, param);
}

rfUint8 set_parameter_by_name(scanner_base_t *device, const char *param_name,
                              va_list value) {
  return rf60x_set_parameter_by_name(device->rf60x, param_name, value);
}

rfUint8 *get_measure_uart(scanner_base_t *device) { //TODO сделать очистку памяти
  rfUint8 *raw_measure = rf60x_get_measure_uart(device->rf60x);
  if (raw_measure == NULL) return NULL;




  return raw_measure;
}

rfBool send_command_uart(scanner_base_t *device, rf60x_command_t cmd)
{
    return rf60x_send_command(device->rf60x,cmd);
}

rf60x_uart_hello_t *hello_msg_uart(scanner_base_t *device)
{
    return rf60x_hello_msg_uart(device->rf60x);
}
