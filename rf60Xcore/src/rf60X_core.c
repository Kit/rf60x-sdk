#include "rf60X_core.h"
#include "endian_conv.h"


rfUint32 core_version()
{
    return 1;
}

void init_platform_dependent_methods(
    memory_platform_dependent_methods_t* memory_methods,
    iostream_platform_dependent_methods_t* iostream_methods,
    network_platform_dependent_methods_t* network_methods,
    network_platform_dependent_settings_t* adapter_settings,
    uart_platform_dependent_methods_t *uart_methods)
{
    init_memory_platform(memory_methods);
    init_iostream_platform(iostream_methods);
    init_network_platform(network_methods);
    init_uart_platform(uart_methods);
    set_adapter_settings(adapter_settings->host_mask, adapter_settings->host_ip_addr);
    determine_endianess();
}
