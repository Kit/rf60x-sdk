#include <stdarg.h>

#include "custom_string.h"
#include "iostream_platform.h"
#include "network.c"
#include "rf60X_devices.h"
#include "rf60X_protocol.h"
#define RF60X_API_VERSION 0x14010a00  // yy.mm.dd.build

#define RF60X_RECV_TIMEOUT 100

#define RF60X_ETHERNET_PORT (603)

#define RF60X_PACKET_UDP_SIZE (512)

#define RF60X_PROTOCOL_USER_PARAMS_PAYLOAD_SIZE (47)

#define INVALID_HANDLE_VALUE 0xFFFFFFFFF

rfUint32 rf60X_api_version() { return RF60X_API_VERSION; }

int rf60X_mutex_lock() {
  //    return pthread_mutex_lock(&_mutex);
  return 0;
}

int rf60X_mutex_trylock() {
  //    rfInt error = pthread_mutex_trylock(&_mutex);
  //    if (error == 0) {
  //        /*... have the lock */
  //        return error;
  //    } else if (error == EBUSY) {
  //        /*... failed to get the lock because another thread holds lock */
  //        return error;
  //    } else if (error == EOWNERDEAD) {
  //        /*... got the lock, but the critical section state may not be
  //        consistent */ return error;
  //    } else {
  //        switch (error) {
  //        case EAGAIN:
  //            /*... recursively locked too many times */
  //            return error;
  //            break;
  //        case EINVAL:
  //            /*... thread priority higher than mutex priority ceiling */
  //            return error;
  //            break;
  //        case ENOTRECOVERABLE:
  //            /*... mutex suffered EOWNERDEAD, and is no longer consistent */
  //            return error;
  //            break;
  //        default:
  //            /*...some other as yet undocumented failure reason */
  //            return error;
  //            break;
  //        }
  //    }
  //    return error;
  return 0;
}

int rf60X_mutex_unlock() {
  //    return pthread_mutex_unlock(&_mutex);
  return 0;
}

rfBool rf60x_connect_udp(rf60x_t *scanner) {
  rfUint32 recv_addr = 0;


  scanner->m_data_sock = network_platform.network_methods.create_udp_socket();
  if (scanner->m_data_sock != (void *)SOCKET_ERROR) {
   int nret = 1;
    network_platform.network_methods.set_reuseaddr_socket_option(
        scanner->m_data_sock);

    network_platform.network_methods.set_socket_recv_timeout(
        scanner->m_data_sock, (rfUint16)RF60X_RECV_TIMEOUT);




    //recv_addr.sin_addr = RF_INADDR_ANY;



    nret = network_platform.network_methods.socket_bind(scanner->m_data_sock,
                                                         INADDR_ANY,  (rfUint16)RF60X_ETHERNET_PORT);
    if (nret == RF_SOCKET_ERROR)
    {
        network_platform.network_methods.close_socket(scanner->m_data_sock);
        scanner->m_data_sock = NULL;
        return FALSE;
    }
  }
  else
  {
      iostream_platform.trace_error("Create data socket error");
      return FALSE;
  }



  return 0;
}

void rf60x_disconnect_udp(rf60x_t *scanner) {
  if (scanner->m_data_sock != NULL) {
    network_platform.network_methods.close_socket(scanner->m_data_sock);
    scanner->m_data_sock = NULL;
  }
}

rfUint8 *rf60x_get_measure_udp(rf60x_t *scanner) {
  rfSize RX_SIZE = (rfUint16)RF60X_PACKET_UDP_SIZE;
  rfUint8 *RX = (rfUint8 *)memory_platform.rf_calloc(1, RX_SIZE);

  int nret = network_platform.network_methods.recv_data(
      scanner->m_data_sock, RX, (rfUint16)RF60X_PACKET_UDP_SIZE);

  if (nret < RF60X_PACKET_UDP_SIZE) {
    memory_platform.rf_free(RX);
    return NULL;
  }

  return (rfUint8 *)RX;
}

rfBool rf60x_open_port_uart(rf60x_t *scanner) {
  scanner->handlePort =
      (void *)uart_platform.uart_methods.create_file(scanner->portName);

  if ((rfSize)INVALID_HANDLE_VALUE == (rfSize)scanner->handlePort)
    return -1;

  else
    return 0;
}

void rf60x_config_port_uart(rf60x_t *scanner) {
  uart_platform.uart_methods.change_paramaters(scanner->handlePort,
                                               scanner->uart_info);
}

rfUint8 *rf60x_get_measure_uart(rf60x_t *scanner) {
    rfUint32 RX_SIZE = 12;
  rfUint8 *RX = (rfUint8 *)memory_platform.rf_calloc(1, RX_SIZE);

  if (uart_platform.uart_methods.recv_data_uart(scanner->handlePort, (void *)RX,
                                                RX_SIZE) != RX_SIZE) {
    memory_platform.rf_free(RX);
    return NULL;
  }
  return RX;
}

// TODO сделать отправку любых команд. Для теста сейчас делаю запуск остановки и
// запуск потока
rfBool rf60x_send_command(rf60x_t *scanner, rf60x_command_t cmd) {
  unsigned char ucBuffer[16];

  memory_platform.rf_memset(ucBuffer, 0, 16);
  ucBuffer[0] =
      (unsigned char)(scanner->user_params.network_address_uart & 0x7F);
  ucBuffer[1] = 0x80 | cmd;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 2) != 2)
    return -1;

  return 1;
}

void rf60x_close_port_uart(rf60x_t *scanner) {
  uart_platform.uart_methods.close_file(scanner->handlePort);
  memory_platform.rf_free(scanner->uart_info);
}

parameter_t *create_parameter_from_type(const rfChar *type) {
  parameter_t *p = NULL;
  if (rf_strcmp(pvtKey[PVT_BYTE], type) ==
      0) {  // TODO strcmp нужно заменить на rf_strcmp
    p = memory_platform.rf_calloc(1, sizeof(parameter_t));
    p->rawData = memory_platform.rf_calloc(1, sizeof(rfByte));
    p->base.type = pvtKey[PVT_BYTE];
  }
  return p;
}

rfBool rf60x_read_params_from_scanner_uart(rf60x_t *scanner) {
  unsigned char ucBuffer[4];
  unsigned char ucResultBuffer[2];
  ucBuffer[0] =
      (unsigned char)(1 & 0xFF);  // TODO надо вынести в общую структуру. Чтобы
  // пользователи могли сами устанавливать адрес.
  ucBuffer[1] = 0x80 | 0x02;  // TODO вынести в отдельные команды

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x00;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.user_power =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x01;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.power_analog_output =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x02;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.control_of_averaging =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x03;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.network_address_uart =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x04;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.baud_rate_uart =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x06;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.number_of_averaged_values =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x08;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_of_the_sampling_period =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x09;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_of_the_sampling_period =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0A;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_of_max_integration_time =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x0B;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_of_max_integration_time =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0C;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_for_the_beginning_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x0D;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_for_the_beginning_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0E;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_for_the_end_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  ucBuffer[2] = 0x80 | 0x0F;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_for_the_end_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x10 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x10 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.time_lock_of_result =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x17 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x17 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_of_a_zero_point =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x18 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x18 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_of_a_zero_point =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x20 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x20 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.data_transfer_rate_via_CAN_interface =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x22 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x22 >> 4) & 0x0F);
  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_of_standard_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x23 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x23 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_of_standard_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x24 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x24 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.zero_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x25 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x25 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.first_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x26 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x26 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.second_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x27 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x27 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.third_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x28 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x28 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.CAN_interface_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x29 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x29 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.CAN_interface_power_switch =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x6C;
  ucBuffer[3] = 0x80;

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.zero_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6D & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6D >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.first_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6E & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6E >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.second_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6F & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6F >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.third_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x70 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x70 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.zero_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x71 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x71 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.first_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x72 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x72 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.second_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x73 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x73 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.third_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x74 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x74 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.zero_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x75 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x75 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.first_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x76 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x76 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.second_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x77 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x77 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.third_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x78 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x78 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.zero_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x79 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x79 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.first_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7A & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7A >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.second_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7B & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7B >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.third_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x7C & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7C >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.low_byte_of_the_number_of_measurements_in_the_packet =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7D & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7D >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.high_byte_of_the_number_of_measurements_in_the_packet =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x88 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x88 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.eth_interface_on_off =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x89 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x89 >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.auto_of_the_stream =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x8A & 0x0F);
  ucBuffer[3] = 0x80 | ((0x8A >> 4) & 0x0F);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 4) != 4)
    return -1;

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);

  scanner->user_params.protocols_interface =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////

  vector_init(&scanner->params_list);

  rfUint32 index = 0;

  ///////////////////////////////////1///////////////////////////
  parameter_t *p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[USER_POWER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 0;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.user_power;
  p->val_uint8->min = 0;
  p->val_uint8->max = 1;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////2///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[POWER_ANALOG_OUTPUT];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 1;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.power_analog_output;
  p->val_uint8->min = 0;
  p->val_uint8->max = 1;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////3///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[CONTROL_OF_AVERAGING];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 2;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.control_of_averaging;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////4///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[NETWORK_ADDRESS_UART];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 3;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.network_address_uart;
  p->val_uint8->min = 1;
  p->val_uint8->max = 127;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 1;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////5///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[BAUD_RATE_UART];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 4;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.baud_rate_uart;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 25;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////6///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[NUMBER_OF_AVERAGED_VALUES];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 5;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.number_of_averaged_values;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////7///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[LOW_BYTE_OF_THE_SAMPLING_PERIOD];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 6;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.low_byte_of_the_sampling_period;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////8///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[HIGH_BYTE_OF_THE_SAMPLING_PERIOD];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 7;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.high_byte_of_the_sampling_period;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////9///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[LOW_BYTE_OF_MAX_INTEGRATION_TIME];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 8;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.low_byte_of_max_integration_time;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////10///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[HIGH_BYTE_OF_MAX_INTEGRATION_TIME];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 9;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.high_byte_of_max_integration_time;
  p->val_uint8->min = 1;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////11///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name =
      parameter_names_array[LOW_BYTE_FOR_THE_BEGINNING_OF_ANALOG_OUT_RANGE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 10;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.low_byte_for_the_beginning_of_analog_out_range;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////12///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name =
      parameter_names_array[HIGH_BYTE_FOR_THE_BEGINNING_OF_ANALOG_OUT_RANGE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 11;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.high_byte_for_the_beginning_of_analog_out_range;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////13///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name =
      parameter_names_array[LOW_BYTE_FOR_THE_END_OF_ANALOG_OUT_RANGE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 12;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.low_byte_for_the_end_of_analog_out_range;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////14///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name =
      parameter_names_array[HIGH_BYTE_FOR_THE_END_OF_ANALOG_OUT_RANGE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 13;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.high_byte_for_the_end_of_analog_out_range;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////15///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[TIME_LOCK_OF_RESULT];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 14;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.time_lock_of_result;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////16///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[LOW_BYTE_OF_A_ZERO_POINT];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 15;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.low_byte_of_a_zero_point;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////17///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[HIGH_BYTE_OF_A_ZERO_POINT];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 16;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.high_byte_of_a_zero_point;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////18///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[DATA_TRANSFER_RATE_VIA_CAN_INTERFACE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 17;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.data_transfer_rate_via_CAN_interface;
  p->val_uint8->min = 10;
  p->val_uint8->max = 200;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////19///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[LOW_BYTE_OF_STANDARD_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 18;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.low_byte_of_standard_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////20///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[HIGH_BYTE_OF_STANDARD_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 19;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.high_byte_of_standard_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////21///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ZERO_BYTE_OF_EXTENDED_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 20;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.zero_byte_of_extended_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////22///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[FIRST_BYTE_OF_EXTENDED_IDENTIFIE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 21;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.first_byte_of_extended_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////23///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[SECOND_BYTE_OF_EXTENDED_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 22;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.second_byte_of_extended_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////24///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[THIRD_BYTE_OF_EXTENDED_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 23;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.third_byte_of_extended_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////25///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[CAN_INTERFACE_IDENTIFIER];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 24;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.CAN_interface_identifier;
  p->val_uint8->min = 0;
  p->val_uint8->max = 1;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////26///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[CAN_INTERFACE_POWER_SWITCH];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 25;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.CAN_interface_power_switch;
  p->val_uint8->min = 0;
  p->val_uint8->max = 1;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////27///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ZERO_BYTE_OF_DESTINATION_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 26;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.zero_byte_of_destination_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////28///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[FIRST_BYTE_OF_DESTINATION_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 27;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.first_byte_of_destination_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////29///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[SECOND_BYTE_OF_DESTINATION_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 28;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.second_byte_of_destination_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////30///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[THIRD_BYTE_OF_DESTINATION_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 29;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.third_byte_of_destination_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////31///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ZERO_BYTE_OF_GATEWAY_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 30;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.zero_byte_of_gateway_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////32///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[FIRST_BYTE_OF_GATEWAY_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 31;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.first_byte_of_gateway_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////33///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[SECOND_BYTE_OF_GATEWAY_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 32;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.second_byte_of_gateway_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////34///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[THIRD_BYTE_OF_GATEWAY_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 33;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.third_byte_of_gateway_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////35///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ZERO_BYTE_OF_SUBNET_MASK];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 34;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.zero_byte_of_subnet_mask;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////36///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[FIRST_BYTE_OF_SUBNET_MASK];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 35;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.first_byte_of_subnet_mask;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////37///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[SECOND_BYTE_OF_SUBNET_MASK];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 36;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.second_byte_of_subnet_mask;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////38///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[THIRD_BYTE_OF_SUBNET_MASK];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 37;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.third_byte_of_subnet_mask;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////39///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ZERO_BYTE_OF_SOURCE_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 38;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.zero_byte_of_source_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////40///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[FIRST_BYTE_OF_SOURCE_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 39;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.first_byte_of_source_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////41///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[SECOND_BYTE_OF_SOURCE_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 40;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.second_byte_of_source_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////42///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[THIRD_BYTE_OF_SOURCE_IP_ADDRESS];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 41;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.third_byte_of_source_ip_address;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////43///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array
      [LOW_BYTE_OF_THE_NUMBER_OF_MEASUREMENTS_IN_THE_PACKET];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 42;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params.low_byte_of_the_number_of_measurements_in_the_packet;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////44///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array
      [HIGH_BYTE_OF_THE_NUMBER_OF_MEASUREMENTS_IN_THE_PACKET];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 43;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value =
      scanner->user_params
          .high_byte_of_the_number_of_measurements_in_the_packet;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////45///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[ETH_INTERFACE_ON_OFF];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 44;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.eth_interface_on_off;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////46///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[AUTO_OF_THE_STREAM];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 45;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.auto_of_the_stream;
  p->val_uint8->min = 0;
  p->val_uint8->max = 255;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 0;
  vector_add(scanner->params_list, p);

  ///////////////////////////////////47///////////////////////////

  p = create_parameter_from_type(pvtKey[PVT_BYTE]);
  p->base.name = parameter_names_array[PROTOCOLS_INTERFACE];
  p->base.access = patKey[PAT_WRITE];
  p->base.index = index++;
  p->base.offset = 46;
  p->base.size = 1;
  p->base.units = "";

  p->val_uint8->value = scanner->user_params.protocols_interface;
  p->val_uint8->min = 1;
  p->val_uint8->max = 1;
  p->val_uint8->step = 0;
  p->val_uint8->defValue = 1;
  vector_add(scanner->params_list, p);

  return 0;
}

rfBool rf60x_write_params_to_scanner_uart(rf60x_t *scanner) {
  rfUint8 payload[RF60X_PROTOCOL_USER_PARAMS_PAYLOAD_SIZE];

  rf60x_protocol_old_pack_payload_msg_to_user_params_packet(
      payload, scanner->params_list);

  memory_platform.rf_memcpy(&scanner->user_params, payload, sizeof(payload));

  unsigned char ucBuffer[16];
  memory_platform.rf_memset(ucBuffer, 0, 16);
  unsigned char ucResultBuffer[2]={0};
  ucBuffer[0] =
      (unsigned char)(1 & 0xFF);  // TODO надо вынести в общую структуру. Чтобы
  // пользователи могли сами устанавливать адрес.
  ucBuffer[1] = 0x80 | 0x03;  // TODO вынести в отдельные команды
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x00;
  ucBuffer[3] = 0x80;
  ucBuffer[4] = 0x80 | (scanner->user_params.user_power & 0x000F);
  ucBuffer[5] = 0x80 | ((scanner->user_params.baud_rate_uart & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /*uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);*/

  scanner->user_params.user_power =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x01;
  ucBuffer[3] = 0x80;
  ucBuffer[4] = 0x80 | (scanner->user_params.power_analog_output & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.power_analog_output & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;
  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.power_analog_output =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x02;
  ucBuffer[3] = 0x80;
  ucBuffer[4] = 0x80 | (scanner->user_params.control_of_averaging & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.control_of_averaging & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;
  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);*/

  scanner->user_params.control_of_averaging =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x03;
  ucBuffer[3] = 0x80;
  ucBuffer[4] = 0x80 | (scanner->user_params.network_address_uart & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.network_address_uart & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;
  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                              (void *)&ucResultBuffer, 2);*/

  scanner->user_params.network_address_uart =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////

  ucBuffer[2] = 0x80 | 0x04;
  ucBuffer[3] = 0x80;
  ucBuffer[4] = 0x80 | (scanner->user_params.baud_rate_uart & 0x000F);
  ucBuffer[5] = 0x80 | ((scanner->user_params.baud_rate_uart & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.baud_rate_uart =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x06;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.number_of_averaged_values & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.number_of_averaged_values & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                            (void *)&ucResultBuffer, 2);*/

  scanner->user_params.number_of_averaged_values =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x08;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.low_byte_of_the_sampling_period & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.low_byte_of_the_sampling_period & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_of_the_sampling_period =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x09;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.high_byte_of_the_sampling_period & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.high_byte_of_the_sampling_period & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_of_the_sampling_period =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0A;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.low_byte_of_max_integration_time & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.low_byte_of_max_integration_time & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_of_max_integration_time =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x0B;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.high_byte_of_max_integration_time & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.high_byte_of_max_integration_time & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_of_max_integration_time =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0C;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.low_byte_for_the_beginning_of_analog_out_range &
       0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.low_byte_for_the_beginning_of_analog_out_range &
        0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_for_the_beginning_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | 0x0D;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.high_byte_for_the_beginning_of_analog_out_range &
       0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.high_byte_for_the_beginning_of_analog_out_range &
        0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_for_the_beginning_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x0E;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.low_byte_for_the_end_of_analog_out_range & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.low_byte_for_the_end_of_analog_out_range &
               0x00F0) >>
              4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_for_the_end_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  ucBuffer[2] = 0x80 | 0x0F;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.high_byte_for_the_end_of_analog_out_range & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.high_byte_for_the_end_of_analog_out_range &
               0x00F0) >>
              4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_for_the_end_of_analog_out_range =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x10 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x10 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.time_lock_of_result & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.time_lock_of_result & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.time_lock_of_result =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x17 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x17 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.low_byte_of_a_zero_point & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.low_byte_of_a_zero_point & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_of_a_zero_point =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x18 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x18 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.high_byte_of_a_zero_point & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.high_byte_of_a_zero_point & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_of_a_zero_point =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x20 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x20 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.data_transfer_rate_via_CAN_interface & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.data_transfer_rate_via_CAN_interface & 0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.data_transfer_rate_via_CAN_interface =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x22 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x22 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.low_byte_of_standard_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.low_byte_of_standard_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_of_standard_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x23 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x23 >> 4) & 0x0F);
  ucBuffer[3] = 0x80 | ((0x22 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.high_byte_of_standard_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.high_byte_of_standard_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_of_standard_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x24 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x24 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.zero_byte_of_extended_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.zero_byte_of_extended_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.zero_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x25 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x25 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.first_byte_of_extended_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.first_byte_of_extended_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.first_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x26 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x26 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.second_byte_of_extended_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.second_byte_of_extended_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.second_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x27 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x27 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.third_byte_of_extended_identifier & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.third_byte_of_extended_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.third_byte_of_extended_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x28 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x28 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.CAN_interface_identifier & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.CAN_interface_identifier & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.CAN_interface_identifier =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x29 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x29 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.CAN_interface_power_switch & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.CAN_interface_power_switch & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.CAN_interface_power_switch =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | 0x6C;
  ucBuffer[3] = 0x80;
  ucBuffer[4] =
      0x80 | (scanner->user_params.zero_byte_of_destination_ip_address & 0x0F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.zero_byte_of_destination_ip_address & 0xF0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.zero_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6D & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6D >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.first_byte_of_destination_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.first_byte_of_destination_ip_address & 0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.first_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6E & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6E >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.second_byte_of_destination_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.second_byte_of_destination_ip_address & 0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.second_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x6F & 0x0F);
  ucBuffer[3] = 0x80 | ((0x6F >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 |
      (scanner->user_params.third_byte_of_destination_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.third_byte_of_destination_ip_address & 0x00F0) >>
       4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.third_byte_of_destination_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x70 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x70 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.zero_byte_of_gateway_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.zero_byte_of_gateway_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/
  scanner->user_params.zero_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x71 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x71 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.first_byte_of_gateway_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.first_byte_of_gateway_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.first_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x72 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x72 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.second_byte_of_gateway_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.second_byte_of_gateway_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.second_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x73 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x73 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.third_byte_of_gateway_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.third_byte_of_gateway_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.third_byte_of_gateway_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x74 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x74 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.zero_byte_of_subnet_mask & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.zero_byte_of_subnet_mask & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/
  scanner->user_params.zero_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x75 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x75 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.first_byte_of_subnet_mask & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.first_byte_of_subnet_mask & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.first_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x76 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x76 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.second_byte_of_subnet_mask & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.second_byte_of_subnet_mask & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.second_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x77 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x77 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.third_byte_of_subnet_mask & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.third_byte_of_subnet_mask & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.third_byte_of_subnet_mask =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x78 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x78 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.zero_byte_of_source_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.zero_byte_of_source_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.zero_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x79 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x79 >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.first_byte_of_source_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.first_byte_of_source_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.first_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7A & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7A >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.second_byte_of_source_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.second_byte_of_source_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.second_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7B & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7B >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params.third_byte_of_source_ip_address & 0x000F);
  ucBuffer[5] =
      0x80 |
      ((scanner->user_params.third_byte_of_source_ip_address & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.third_byte_of_source_ip_address =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x7C & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7C >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params
                  .low_byte_of_the_number_of_measurements_in_the_packet &
              0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params
                   .low_byte_of_the_number_of_measurements_in_the_packet &
               0x00F0) >>
              4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.low_byte_of_the_number_of_measurements_in_the_packet =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));

  ucBuffer[2] = 0x80 | (0x7D & 0x0F);
  ucBuffer[3] = 0x80 | ((0x7D >> 4) & 0x0F);
  ucBuffer[4] =
      0x80 | (scanner->user_params
                  .high_byte_of_the_number_of_measurements_in_the_packet &
              0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params
                   .high_byte_of_the_number_of_measurements_in_the_packet &
               0x00F0) >>
              4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.high_byte_of_the_number_of_measurements_in_the_packet =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x88 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x88 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.eth_interface_on_off & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.eth_interface_on_off & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.eth_interface_on_off =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x89 & 0x0F);
  ucBuffer[3] = 0x80 | ((0x89 >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.auto_of_the_stream & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.auto_of_the_stream & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.auto_of_the_stream =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////
  ucBuffer[2] = 0x80 | (0x8A & 0x0F);
  ucBuffer[3] = 0x80 | ((0x8A >> 4) & 0x0F);
  ucBuffer[4] = 0x80 | (scanner->user_params.protocols_interface & 0x000F);
  ucBuffer[5] =
      0x80 | ((scanner->user_params.protocols_interface & 0x00F0) >> 4);

  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 6) != 6)
    return -1;

  /* uart_platform.uart_methods.recv_data_uart(scanner->handlePort,
                                             (void *)&ucResultBuffer, 2);*/

  scanner->user_params.protocols_interface =
      ((ucResultBuffer[0] & 0x0F) | ((ucResultBuffer[1] & 0x0F) << 4));
  /////////////////////////////////////////////////////////////////////////////////////////////

  return 0;
}

rf60x_uart_hello_t *rf60x_hello_msg_uart(rf60x_t *scanner) {
  unsigned char ucBuffer[2];
  ucBuffer[0] = (unsigned char)(1 & 0xFF);
  ucBuffer[1] = 0x80 | 0x01;
  if (uart_platform.uart_methods.send_data_uart(scanner->handlePort,
                                                (void *)&ucBuffer, 2) == 2)
    return NULL;

  rf60x_uart_hello_t *uart_hello = memory_platform.rf_calloc(
      1, sizeof(rf60x_uart_hello_t));  // TODO тоже проблема с утечкой памяти

  rfByte *RX = memory_platform.rf_calloc(1, 8);

  uart_platform.uart_methods.recv_data_uart(scanner->handlePort, RX, 8);

  uart_hello->deviceType = (RX[0] & 0x0F) | ((RX[1] & 0x0F) << 4);
  uart_hello->deviceModificaton = (RX[2] & 0x0F) | ((RX[3] & 0x0F) << 4);
  uart_hello->deviceSerial = (RX[4] & 0x0F) | ((RX[5] & 0x0F) << 4) |
                             ((RX[6] & 0x0F) << 8) | ((RX[7] & 0x0F) << 12);
  uart_hello->deviceMaxDistance = (RX[8] & 0x0F) | ((RX[9] & 0x0F) << 4) |
                                  ((RX[10] & 0x0F) << 8) |
                                  ((RX[11] & 0x0F) << 12);
  uart_hello->deviceRange = (RX[12] & 0x0F) | ((RX[13] & 0x0F) << 4) |
                            ((RX[14] & 0x0F) << 8) | ((RX[15] & 0x0F) << 12);

  memory_platform.rf_free(RX);

  if (uart_hello == NULL) {
    memory_platform.rf_free(uart_hello);
    return NULL;
  }
  return uart_hello;
}

parameter_t *rf60x_get_parameter(rf60x_t *scanner, const rfChar *param_name) {
  for (rfSize i = 0; i < vector_count(scanner->params_list); i++) {
    parameter_t *p = vector_get(scanner->params_list, i);
    if (rf_strcmp(p->base.name, param_name) == 0) {
      return p;
    }
  }
  return NULL;
}

rfUint8 rf60x_set_parameter(rf60x_t *scanner, parameter_t *param) {
  for (rfSize i = 0; i < vector_count(scanner->params_list); i++) {
    parameter_t *p = vector_get(scanner->params_list, i);
    if (rf_strcmp(p->base.name, param->base.name) == 0) {
      if (rf_strcmp(p->base.type, pvtKey[PVT_BYTE]) == 0) {
        p->val_uint8->value = param->val_uint8->value;
        return 0;
      }
    }
  }
  return 1;
}

rfUint8 rf60x_set_parameter_by_name(rf60x_t *scanner, const rfChar *param_name,
                                    va_list value) {
  for (rfSize i = 0; i < vector_count(scanner->params_list); i++) {
    parameter_t *p = vector_get(scanner->params_list, i);
    if (rf_strcmp(p->base.name, param_name) == 0) {
      if (rf_strcmp(p->base.type, pvtKey[PVT_BYTE]) == 0) {
        p->val_uint8->value = va_arg(value, rfInt);
        return 0;
      }
    }
  }
  return 1;
}
