#ifndef RF60X_DEVICES_H
#define RF60X_DEVICES_H

#include "custom_vector.h"
#include "memory_platform.h"
#include "rf60X_config.h"
#include "rf60X_types.h"
#include "uart_platform.h"

typedef struct {
  rf60x_uart_params_t user_params;

  rf_DCB *uart_info;
  char portName[32];
  void *m_data_sock;
  void *handlePort;
  vector_t *params_list;

} rf60x_t;

/*! Return rf60X api version
 */
rfUint32 rf60X_old_api_version();

/**
 * @brief rf60X_mutex_lock
 * @return
 */
int rf60X_mutex_lock();

/**
 * @brief rf60X_mutex_trylock
 * @return
 */
int rf60X_mutex_trylock();

/**
 * @brief rf60X_mutex_unlock
 * @return
 */
int rf60X_mutex_unlock();

/**
 * @brief rf60x_connect - Establish connection to the device
 * @param scanner - ptr to device
 * @return true on success
 */
rfBool rf60x_connect_udp(rf60x_t *scanner);

/**
 * @brief rf60x_disconnect - Close connection to the device
 * @param scanner - ptr to device
 */
void rf60x_disconnect_udp(rf60x_t *scanner);

/**
 * @brief rf60x_get_measure - Get measurement from scanner's data stream
 * @param scanner - ptr to device
 * @return ptr to pointer on the structure
 */
rfUint8 *rf60x_get_measure_udp(rf60x_t *scanner);

/**
 * @brief rf60x_open_port_uart - Opens COM port for communication with the
 * device
 * @param scanner - ptr to device
 * @return status of implementation of the method
 */

rfBool rf60x_open_port_uart(rf60x_t *scanner);

/**
 * @brief rf60x_config_port_uart - Configuring the port configuration to work
 * with it
 * @param scanner - ptr to device
 */
void rf60x_config_port_uart(rf60x_t *scanner);

// TODOD добавить описание здесь и в rf60x_sdk.h

rfUint8 *rf60x_get_measure_uart(rf60x_t *scanner);
/**
 * @brief rf60x_config_port_uart - Открывает COM- порт для связи с устройством
 * @param scanner - ptr to device
 */
void rf60x_close_port_uart(rf60x_t *scanner);

/**
 * @brief rf60x_uart_read_params_from_scanner - Read parameters from device to
 * rfInternal structure. This structure is accessible via get_params() function
 * @param scanner - ptr to scanner
 * @return 0 on success
 */
rfBool rf60x_read_params_from_scanner_uart(rf60x_t *scanner);

/**
 * @brief rf60x_uart_write_params_to_scanner - Write current parameters to
 * device.
 * @param scanner - ptr to scanner
 * @return 0 on success
 */
rfBool rf60x_write_params_to_scanner_uart(rf60x_t *scanner);

/**
 * @brief rf60x_hello_msg_uart - Sends a request to com port with command 0x01
 * to identify the device
 * @param scanner - ptr to scanner
 * @return param on success, else - null
 */

rfBool rf60x_send_command(rf60x_t *scanner, rf60x_command_t cmd);

rf60x_uart_hello_t *rf60x_hello_msg_uart(rf60x_t *scanner);

/**
 * @brief rf60x_get_parameter - Search parameters by his name
 * @param scanner - ptr to scanner
 * @return param on success, else - null
 */
parameter_t *rf60x_get_parameter(rf60x_t *scanner, const rfChar *param_name);

/**
 * @brief rf60x_set_parameter - set parameters
 * @param scanner - ptr to scanner
 * @param param - ptr to parameter
 * @return 0 on success
 */
rfUint8 rf60x_set_parameter(rf60x_t *scanner, parameter_t *param);

/**
 * @brief rf60x_set_parameter_by_name - set parameter by his name
 * @param scanner - ptr to scanner
 * @param param_name - param name
 * @param value - setting value
 * @return 0 on success
 */
rfUint8 rf60x_set_parameter_by_name(rf60x_t *scanner, const rfChar *param_name,
                                    va_list value);

typedef struct {
  scanner_types_t type;
  union {
    rf60x_t *rf60x;
  };
} scanner_base_t;

#endif  // RF60X_DEVICES_H
